# coding: utf8

import requests
import json
import time
import os
from subprocess import call


#############
## GLOBALS ##
#############


global consumer_key, max_retry, priority_tags
consumer_key = ''
max_retry = 5
priority_tags = ['1', '2', '3', '4', '5', 'a']
browser_command = ''
useless_callback='https://www.google.fr/search?q=answer+to+life,+the+universe,+and+everything&gws_rd=cr&ei=z2CiV5ShGcv2aKSGrvgP'


###############
## FUNCTIONS ##
###############


def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def login():
    headers = {'X-Accept': 'application/json'}
    payload = {'consumer_key': consumer_key, 'redirect_uri': 'none'}
    r = requests.post('https://getpocket.com/v3/oauth/request', headers=headers, data=payload)
    r.encoding = 'utf-8'
    access_token = r.json()['code']

    authentication_uri = 'https://getpocket.com/auth/authorize?request_token=' + access_token + '&redirect_uri=' + useless_callback
    payload = {'consumer_key': consumer_key, 'code': access_token}

    for x in range(1, max_retry):
        call(['open', '-a', 'Google Chrome', authentication_uri])
        r = requests.post('https://getpocket.com/v3/oauth/authorize', headers=headers, data=payload)

        if (r.status_code != 403):
            user_access_token = r.json()['access_token']
            break
        elif(x == max_retry):
            print('Max retry (' + ') reached for 403.')
            exit(3)

    return user_access_token.encode('utf-8')

def load_all_articles(access_token):
    headers = {'X-Accept': 'application/json'}
    payload = {'consumer_key': consumer_key, 'access_token': access_token, 'detailType': 'complete'}
    content = requests.post('https://getpocket.com/v3/get', headers=headers, data=payload).json()['list']

    global_word_count = 0
    number_word_count = 0
    initial_not_priorized_nb = 0
    articles = []

    initial_nb = len(content)

    for article in content.itervalues():
        new_article_format = {}
        new_article_format['id'] = article['item_id'].encode('utf-8')
        new_article_format['title'] = article['given_title'].encode('utf-8')
        new_article_format['url'] = article['given_url'].encode('utf-8')
        new_article_format['tags'] = [tag.encode('utf-8') for tag in article['tags'].keys()] if 'tags' in article else []
        new_article_format['length'] = int(article['word_count'].encode('utf-8')) if 'word_count' in article else 0

        if (len([priority_tag for priority_tag in priority_tags if priority_tag in new_article_format['tags']]) > 0):
            continue

        articles.append(new_article_format)
        global_word_count += new_article_format['length'] if new_article_format['length'] > 0 else 0
        number_word_count += 1 if new_article_format['length'] > 0 else number_word_count
        initial_not_priorized_nb += 1

    average_word_count = int(global_word_count / number_word_count) if number_word_count > 0 else 0
    return (articles, initial_nb, average_word_count, initial_not_priorized_nb)


def update_tags(access_token, articles, nb_articles):
    headers = {'X-Accept': 'application/json'}
    count = 1

    for article in articles:
        print('Article n.' + str(count) + '  |  Remaining articles: ' + str(nb_articles - count))
        print('\n')
        print('Title:')
        print(article['title'])
        print
        print('URL:')
        print(article['url'])

        prompt = 'Choose a priority tag [1, 2, 3, 4, 5, a]: '
        tag = raw_input(prompt)

        while (tag not in priority_tags):
            prompt = 'Priority tag [1, 2, 3, 4, 5, a]: '
            tag = raw_input(prompt)

        # ToDo: add a var name for the archive action prompt
        if (tag == 'a'):
            action = [{'action': 'archive', 'item_id': int(article['id'])}]
            payload = {'consumer_key': consumer_key, 'access_token': access_token, 'actions': json.dumps(action)}
            r = requests.post('https://getpocket.com/v3/send', headers=headers, data=payload)
            print(r.status_code)
        else:
            action = [{'action': 'tags_add', 'item_id': int(article['id']), 'tags': tag}]
            payload = {'consumer_key': consumer_key, 'access_token': access_token, 'actions': json.dumps(action)}
            r = requests.post('https://getpocket.com/v3/send', headers=headers, data=payload)

        cls()
        count +=1


##########
## MAIN ##
##########


print('Login in...')
access_token = login()

print('Loading all article... (This may take a while)')
articles, initial_nb, average_word_count, nb_articles = load_all_articles(access_token)

cls()
print('Total library non-read article number: ' + str(initial_nb))

if (len(articles) == 0):
    print('No article without priority tags')
    exit(0)

update_tags(access_token, articles, nb_articles)
exit(0)
