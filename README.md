# Pocket sort helper

If you use [Pocket](https://getpocket.com/) daily and are like I was a few month ago, you might have a bunch of articles. And, de facto, you don't really know what to read when you open your list except the first ones you see.

Well, let's refresh a bit this list by adding some tags, CLI way to save a few hours!

## Disclaimer
This script is tightly coupled to **macOs** and **Google chrome**, sorry about that. I'm thinking about some Windows/*nix check and browser settings among a config file but I don't really have the time for that. I won't mind a PR or two.

See [hard dependencies](hard-dependencies).

And yeah, the code is ugly too, way far from my sofware craftmanship mindset.

## Usage
- Add your own pocket consumer key in `main.py`
- Run:
```shell
python main.py
```
- Log in and authorize this application
- The script may exit at this point, rerun it
- For each article, select a tag (`1` to `5`) or archive it with `a`
- `Ctrl` + `c` when you're done or when you're tired of your 1500+ articles to sort

## Dependencies
- [Python 2.7](https://www.python.org/download/releases/2.7/)

## Hard dependencies
- macOs (tested on Sierra)
- Google chrome (tested on 53.*)

## Some numbers
- Loading 1500 articles: ~30 seconds
- Adding a tag/archive 100 articles: 10 minutes
- It tooks me 2 months to sort 1543 articles